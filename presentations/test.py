class Counter:
    count = 0

    def get_count(self):
        return self.count

    def increment(self):
        self.count += 1


class ConfigurableCounter(Counter):
    print(Counter.count)
